# Tinguely

[![crate](https://img.shields.io/crates/v/mathru.svg)](https://crates.io/crates/tinguely)
[![documentation](https://docs.rs/mathru/badge.svg)](https://docs.rs/mathru)
![minimum rustc 1.46.0](https://img.shields.io/badge/rustc-1.46.0-green.svg)
![maintenance](https://img.shields.io/badge/maintenance-actively--developed-brightgreen.svg)
[![pipeline status](https://gitlab.com/matthiaseiholzer/tinguely/badges/master/pipeline.svg)](https://gitlab.com/matthiaseiholzer/tinguely/-/commits/master)
------------
Tinguely is a machine learning library implemented entirely in Rust.
This library is still in early stages of development.

## Features

Tinguely uses [mathru]([https://crates.io/crates/mathru) for its linear
algebra calculations and optimization algorithms. There is still lot of
room for optimization, but BLAS/LAPACK support is already integrated.

Currently implemented algorithms:
- Clustering
  - K-MEANS
- Regression
  - Linear Regression
- Classification
  - Logistic Regression

The models all provide predict and train methods enforced by the
SupervisedLearn and UnsupervisedLearn traits.

## Usage

Add this to your `Cargo.toml` for the native Rust implementation:

```toml
[dependencies.tinguely]
version = "0.1"
```
Add the following lines to 'Cargo.toml' if the openblas library should be used:

```toml
[dependencies.tinguely]
version = "0.1"
default-features = false
features = "openblas"
```

One of the following implementations for linear algebra can be activated as a feature:
- native: Native Rust implementation(activated by default)
- [openblas](https://www.openblas.net): Optimized BLAS library
- [netlib](https://www.netlib.org): Collection of mathematical software, papers, and databases
- [intel-mkl](https://software.intel.com/content/www/us/en/develop/tools/math-kernel-library.html): Intel Math Kernel Library
- [accelerate](https://developer.apple.com/documentation/accelerate) Make large-scale mathematical computations and image calculations, optimized for high performance and low-energy consumption.(macOS only)


Then import the modules and it is ready to be used.

```rust
use tinguely as tg;
```

## Documentation

See [project page](https://matthiaseiholzer.gitlab.io/mathru) for more information and examples.
The API is documented on [docs.rs](https://docs.rs/mathru).

## License

Licensed under

 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

### Contribution

Any contribution is welcome!
