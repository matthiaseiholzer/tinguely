
#[cfg(test)]
mod svm
{
	use mathru::algebra::linear::{Vector, Matrix};
	use tinguely::classification::SVM;
	use tinguely::model::SupervisedLearn;

	fn compare_vector_epsilon(exp: &Vector<f64>, act: &Vector<f64>, epsilon: f64) -> bool
    {
        let (exp_m, _exp_n): (usize, usize) = exp.dim();
        let (_act_m, _act_n): (usize, usize) = act.dim();

        for i in 0..exp_m
        {
            if (*exp.get(i) - *act.get(i)).abs() > epsilon
            {
            	println!("expected: {}, actual: {}", exp, act);
                return false;
            }
        }

        return true;
    }


	#[test]
	fn fn0()
	{
		let x_train: Matrix<f64> = matrix![0.0, 0.0; 1.0, 1.0];
		let y_train: Vector<f64> = vector![-1.0; 1.0];

		let mut svm: SVM = SVM::new(0.1, 10);

		svm.train(&x_train, &y_train);

		let alpha_hat: Vector<f64> = svm.get_parameter();
		let alpha: Vector<f64> = vector![1.0; 1.0; -1.0];
		assert!(compare_vector_epsilon(&alpha, &alpha_hat, 0.001f64));
	}

	#[test]
	fn fn1()
	{
		let x_train: Matrix<f64> = matrix![0.0, 0.0; 1.0, 1.0; 1.0, 1.0];
		let y_train: Vector<f64> = vector![-1.0; 1.0; 1.0];

		let mut svm: SVM = SVM::new(0.1, 100);

		svm.train(&x_train, &y_train);

		let alpha_hat: Vector<f64> = svm.get_parameter();
		let alpha: Vector<f64> = vector![1.0; 1.0; -1.0];
		assert!(compare_vector_epsilon(&alpha, &alpha_hat, 0.001f64));
	}

	#[test]
	fn fn2()
	{
		let x_train: Matrix<f64> = matrix![0.0, 0.0; 0.0, 1.0];
		let y_train: Vector<f64> = vector![-1.0; 1.0];

		let mut svm: SVM = SVM::new(0.1, 100);

		svm.train(&x_train, &y_train);
		let x = matrix![0.0, 2.0];
		let y = vector![1.0];
		let y_hat: Vector<f64> = svm.predict(&x);

		let alpha: Vector<f64> = vector![0.0; 2.0; -1.0];
		let alpha_hat: Vector<f64> = svm.get_parameter();

		assert!(compare_vector_epsilon(&alpha, &alpha_hat, 0.001f64));
		assert!(compare_vector_epsilon(&y, &y_hat, 0.001f64));
	}

	#[test]
	fn fn3()
	{
		let x_train: Matrix<f64> = matrix![0.5, 0.0; 0.0, 1.0];
		let y_train: Vector<f64> = vector![-1.0; 1.0];

		let mut svm: SVM = SVM::new(0.1, 100);

		svm.train(&x_train, &y_train);
		let x = matrix![1.0, 0.0; 0.0, 2.0];
		let y = vector![-1.0, 1.0];
		let y_hat: Vector<f64> = svm.predict(&x);

		let alpha: Vector<f64> = vector![0.0; 2.0; -1.0];
		let _alpha_hat: Vector<f64> = svm.get_parameter();

		assert!(compare_vector_epsilon(&y, &y_hat, 0.001f64));
	}
}