use mathru::algebra::linear::{Vector, Matrix};
use mathru::optimization::Gradient;
use tinguely::classification::LogisticRegression;
use tinguely::model::SupervisedLearn;

#[test]
fn fn0()
{
	let epsilon: f64 = 0.1;
	let m: usize = 1;
	let n: usize = 1;

	let x: Matrix<f64> = Matrix::new(m, n, vec![2.0]);
	let y: Vector<f64> = Vector::new_column(m, vec![1.0]);

	let optimizer: StochasticGradientDesc = StochasticGradientDesc::new(0.1, 0.8, 100);
	let mut model: LogisticRegression<StochasticGradientDesc> = LogisticRegression::new(optimizer);
	model.train(&x, &y);

	let y_hat: Vector<f64> = model.predict(&x);

	let diff: Vector<f64> = y-y_hat;
	let sum_sqared_diff : f64 = diff.dotp( &diff);

	assert!(sum_sqared_diff.sqrt() < epsilon);;
}

#[test]
fn fn2()
{
	let epsilon: f64 = 0.2;
	let m: usize = 4;
	let n: usize = 1;

	let x: Matrix<f64> = Matrix::new(m, n, vec![1.0, 3.0, 5.0, 7.0]);
	let y: Vector<f64> = Vector::new_column(m, vec![0.0, 0.0, 1.0, 1.0]);

	let optimizer: StochasticGradientDesc = StochasticGradientDesc::new(0.01, 0.0, 600);

	let mut model: LogisticRegression<StochasticGradientDesc> = LogisticRegression::new(optimizer);
	model.train(&x, &y);

	let x_test: Matrix<f64> = Matrix::new(1, n, vec![10.0]);
	let y_test: Vector<f64> = Vector::new_column(1, vec![1.0]);
	let y_hat: Vector<f64> = model.predict(&x_test);

	let diff: Vector<f64> = y_test - y_hat;
	let sum_sqared_diff: f64= diff.dotp( &diff);
	assert!(sum_sqared_diff.sqrt() < epsilon);

}

#[test]
fn fn1()
{
	let epsilon: f64 = 0.1;
	let m: usize = 4;
	let n: usize = 1;

	let x: Matrix<f64> = Matrix::new(m, n, vec![1.0, 3.0, 5.0, 7.0]);
	let y: Vector<f64> = Vector::new_column(m, vec![0.0, 0.0, 1.0, 1.0]);

	let optimizer: StochasticGradientDesc = StochasticGradientDesc::new(0.2, 0.8, 600);
	let mut model: LogisticRegression<StochasticGradientDesc> = LogisticRegression::new(optimizer);
	model.train(&x, &y);

	let y_hat: Vector<f64> = model.predict(&x);

	let diff: Vector<f64> = y - y_hat;
	let sum_sqared_diff: f64= diff.dotp( &diff);
	assert!(sum_sqared_diff.sqrt() < epsilon);
}

#[test]
fn fn3()
{
	let epsilon: f64 = 0.2;
	let m: usize = 4;
	let n: usize = 1;

	let x: Matrix<f64> = Matrix::new(m, n, vec![1.0, 3.0, 5.0, 7.0]);
	let y: Vector<f64> = Vector::new_column(m, vec![0.0, 0.0, 1.0, 1.0]);

	let optimizer: BatchGradientDesc = BatchGradientDesc::new(0.1, 0.8, 500);
	let mut model: LogisticRegression<BatchGradientDesc> = LogisticRegression::new(optimizer);
	model.train(&x, &y);

	let x_s: Matrix<f64> = matrix![0.0; 8.0];
	let y_s: Vector<f64> = vector![0.0; 1.0];
	let y_s_hat: Vector<f64> = model.predict(&x_s);

	let diff: Vector<f64> = y_s - y_s_hat;
	let sum_sqared_diff: f64= diff.dotp( &diff);
	assert!(sum_sqared_diff.sqrt() < epsilon);
}