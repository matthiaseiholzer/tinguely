use mathru as mr;
use tinguely as tg;
use mr::algebra::linear::{Vector, Matrix};
use mathru::optimization::Gradient;
use tg::regression::LinearRegression;
use tg::model::SupervisedLearn;
use mathru::statistics::distrib::{Distribution, Normal, Uniform};

/**
* x_2 = a + b * x_1 * e
*
*
* x_1 \in [0.0, 5.0]
*/
fn generate_data(a: f64, b: f64, samples: usize, sigma: f64) -> (Matrix<f64>, Vector<f64>) {
	assert!(samples > 2, "Number of samples must be greater or equal to two.");
	assert!(sigma >= 0.0f64, "Noise must be non-negative.");

	let uniform: Uniform<f64> = Uniform::new(0.0, 5.0);
	let normal: Normal<f64> = Normal::new(0.0f64, sigma);

	let mut x_1_vec: Vec<f64> = Vec::with_capacity(samples);
	let mut x_2_vec: Vec<f64> = Vec::with_capacity(samples);

	for _ in 0..samples
	{
		let x: f64 = uniform.random();
		let e: f64 = normal.random();

		let y: f64 = a + b * x + e;
		x_1_vec.push(x);
		x_2_vec.push(y);
	}

	let x_1: Matrix<f64> = Matrix::new(samples, 1, x_1_vec);
	let x_2: Vector<f64> = Vector::new_column(samples, x_2_vec);

	return (x_1, x_2)
}

#[test]
fn fn0()
{
	let beta: Vector<f64> = vector![1.0, 2.0];
	let (x, y): (Matrix<f64>, Vector<f64>) = generate_data(*beta.get(0), *beta.get(1), 100, 0.05);

	let optimizer: Gradient<f64> = Gradient::new(0.02, 1000);

	let mut model: LinearRegression<f64> = LinearRegression::new(optimizer);
	model.train(&x, &y);

	assert_relative_eq!(beta, model.parameters().unwrap(), epsilon=0.1)
}
