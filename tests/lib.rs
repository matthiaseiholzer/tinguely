/// Tinguely is a machine learning library written in Rust
///

#[macro_use]
extern crate mathru;

//pub mod classification;
pub mod regression;
pub mod cluster;
