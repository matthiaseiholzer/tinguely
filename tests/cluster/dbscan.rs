#[cfg(test)]
mod dbscan
{
    use tinguely::clustering::{DBSCAN};
    use tinguely::model::UnsupervisedLearn;

    #[test]
    fn dbscan_0()
    {
        let training_data: Vec<(f64, f64)> = vec![  (1.0, 2.0),
                                                    (1.1, 2.2),
                                                    (0.9, 1.9),
                                                    (1.0, 2.1),
                                                    (-2.1, 3.0),
                                                    (-2.3, 3.2),
                                                    (-2.2, 3.1),
                                                    (5.0, 4.1)
                                                    ];

        let dist_func: fn(&(f64, f64), &(f64, f64))->f64 = |&x, &y| { ((y.0 - x.0) * (y.0 -x.0) + (y.1 - x.1) * (y.1 -x.1)).sqrt()};
        let mut model: DBSCAN<(f64, f64)> = DBSCAN::new(0.4, 2, dist_func);
        model.train(&training_data);
        assert_eq!(model.get_number_clusters(), 2);

        let p1: (f64, f64) = (1.0, 2.0);
        let p2: (f64, f64) = (0.9, 2.1);
        let p3: (f64, f64) = (-2.1, 3.0);
        let p4: (f64, f64) = (-5.0, 1.0);

        let labels: Vec<i32> = model.predict(&vec![p1, p2, p3, p4]);
        assert_eq!(labels[0], 0);
        assert_eq!(labels[1], 0);
        assert_eq!(labels[2], 1);
        assert_eq!(labels[3], -1);
    }
}