
extern crate tinguely;

use tinguely::clustering::{KMeans, KMeansInitializer};
use tinguely::model::UnsupervisedLearn;
use mathru::algebra::linear::{Vector, Matrix};

#[test]
fn kmeans0()
{
    let training_data: Matrix<f64> = Matrix::zero(1, 2);

    let mut kmeans: KMeans = KMeans::new(1, 100, KMeansInitializer::Random);
    kmeans.train(&training_data);

    let test_data: Matrix<f64> = Matrix::zero(1, 2);
    let reference: Vector<f64> = Vector::zero(1);
    let result: Vector<f64> = kmeans.predict(&test_data);

    assert_eq!(reference, result);
}

// #[test]
// fn kmeans1()
// {
//     let k: usize = 1;
//     let n: usize = 100;
//     let training_data: Matrix<f64> = Matrix::new(1, 2, vec![1.0, 1.0]);
//
//     let mut kmeans : KMeans = KMeans::new(k, n, KMeansInitializer::Random);
//     kmeans.train(&training_data);
//
//     let test_data: Matrix<f64> = Matrix::new(1, 2, vec![1.0, 1.0]);
//     let reference: Vector<usize> = Vector::new_column(1,vec![0]);
//
//     let result: Vector<usize> = kmeans.predict(&test_data);
//
//     assert_eq!(reference, result);
// }
//
// #[test]
// fn kmeans2()
// {
//     let k: usize = 2;
//     let n: usize = 100;
//     let training_data: Matrix<f64> = Matrix::new(2, 2, vec![1.0, 1.0, 0.0, 0.0]);
//
//     let mut kmeans : KMeans = KMeans::new(k, n, KMeansInitializer::Random);
//     kmeans.train(&training_data);
//
//     let test_data: Matrix<f64> = Matrix::new(1, 2, vec![1.0, 1.0]);
//     let reference: Vector<usize> = Vector::new_column(1,vec![0]);
//
//     let result: Vector<usize> = kmeans.predict(&test_data);
//
//     assert_eq!(reference, result);
// }
//
// #[test]
// fn kmeans3()
// {
//     let k: usize = 2;
//     let n: usize = 100;
//     let training_data: Matrix<f64> = Matrix::new(2, 2, vec![1.0, 1.0, 0.0, 0.0]);
//
//     let mut kmeans : KMeans = KMeans::new(k, n, KMeansInitializer::Random);
//     kmeans.train(&training_data);
//
//     let test_data: Matrix<f64> = Matrix::new(1, 2, vec![0.0, 0.0]);
//     let reference: Vector<usize> = Vector::new_column(1,vec![0]);
//
//     let result: Vector<usize> = kmeans.predict(&test_data);
//
//     assert_eq!(reference, result);
// }
//
// #[test]
// fn kmeans4()
// {
//     let k: usize = 2;
//     let n: usize = 100;
//     let training_data: Matrix<f64> = Matrix::new(2, 2, vec![1.0, 1.0, 0.0, 0.0]);
//
//     let mut kmeans : KMeans = KMeans::new(k, n, KMeansInitializer::Random);
//     kmeans.train(&training_data);
//
//     let test_data: Matrix<f64> = Matrix::new(1, 2, vec![0.5, 0.5]);
//     let reference: Vector<usize> = Vector::new_column(1,vec![0]);
//
//     let result: Vector<usize> = kmeans.predict(&test_data);
//     assert_eq!(reference, result);
// }
//
// #[test]
// fn kmeans_5()
// {
//     let mut model: KMeans = KMeans::new(1, 1, KMeansInitializer::Random);
//     let input: Matrix<f64> = Matrix::new(3, 2, vec![1.0, 2.0, 1.0, 3.0, 1.0, 4.0]);
//     let reference: Vector<usize> = vector![0; 0; 0];
//     model.train(&input);
//
//     let result: Vector<usize> = model.predict(&input);
//     assert_eq!(reference, result);
// }
//
// #[test]
// fn kmeans_6()
// {
//     let mut model: KMeans = KMeans::new(3, 100, KMeansInitializer::Random);
//     let input: Matrix<f64> = Matrix::new(3, 2, vec![1.0, 2.0, 3.0, 3.0, 1.0, 4.0]);
//     let input_s: Matrix<f64> = Matrix::new(1, 2, vec![2.0, 3.0]);
//     let reference: Vector<usize> = vector![0];
//     model.train(&input);
//
//     let result: Vector<usize> = model.predict(&input_s);
//     assert_eq!(reference, result);
// }

