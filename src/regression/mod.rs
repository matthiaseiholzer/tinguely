//! Regression
//!
//! Fore more information: <br>
//! <a href="https://en.wikipedia.org/wiki/Regression_analysis">https://en.wikipedia.org/wiki/Regression_analysis</a>
//!
mod linear_regression;

pub use self::linear_regression::LinearRegression;
