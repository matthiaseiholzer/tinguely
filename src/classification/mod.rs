//! Classification
//!
//! Fore more information: <br>
//! <a href="https://en.wikipedia.org/wiki/Statistical_classification">https://en.wikipedia.org/wiki/Statistical_classification</a>


//mod naivebayes;

//pub use self::naivebayes::NaiveBayes;
mod logistic_regression;
pub use self::logistic_regression::LogisticRegression;

//mod svm;
//pub use self::svm::SVM;

