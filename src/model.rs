
   
pub trait UnsupervisedLearn<T, U>
{
    fn train(self: &mut Self, input: &T);

    fn predict(self: &Self, input: &T) -> U;
}

pub trait SupervisedLearn<T, U>
{
    /// Predict output from inputs.
    fn predict(self: &Self, input: &T) -> Result<U, ()>; // Result<U, &str>;

    /// Train the model using inputs and targets.
    fn train<'a, 'b>(self: &'a mut Self, input: &'b T, target: &'b U); //Result<(), ()>;
}
   
