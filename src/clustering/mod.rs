//! Clustering of unlabeled can be performed with the cluster module.
//!
//! Fore more information: <br>
//! <a href="https://en.wikipedia.org/wiki/Statistical_classification">https://en.wikipedia.org/wiki/Statistical_classification</a>
mod kmeans;
//mod dbscan;

pub use self::kmeans::{KMeans, KMeansInitializer};
//pub use self::dbscan::DBSCAN;
