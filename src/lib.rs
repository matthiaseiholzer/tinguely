#![doc(html_favicon_url = "\">
<script defer src=\"https://cdn.jsdelivr.net/npm/katex@0.10.1/dist/katex.min.js\" integrity=\"sha384-2BKqo+exmr9su6dir+qCw08N2ZKRucY4PrGQPPWU1A7FtlCGjmEGFqXCv5nyM5Ij\" crossorigin=\"anonymous\"></script>
<script>
document.addEventListener(\"DOMContentLoaded\", function () {
	let to_do = [];
	for (let e of document.getElementsByTagName(\"code\")) {
		if (e.classList.contains(\"language-math\")) {
			to_do.push(function () {
				let x = document.createElement('p');
				katex.render(e.innerText, x, {displayMode: true, throwOnError: false});
				e.parentNode.parentNode.replaceChild(x, e.parentNode);
			});
		} else {
			let n = e.nextSibling; let p = e.previousSibling;
			if (n && p && /^\\$/.test(n.data) && /\\$$/.test(p.data)) {
				to_do.push(function () {
					let n = e.nextSibling; let p = e.previousSibling;
					let x = document.createElement('span');
					katex.render(e.innerText, x, {throwOnError: false});
					e.parentNode.replaceChild(x, e);
					n.splitText(1); n.remove();
					p.splitText(p.data.length - 1).remove();
				});
			}
		}
	}
	for (let f of to_do) f();
});
</script>
<link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/katex@0.10.1/dist/katex.min.css\" integrity=\"sha384-dbVIfZGuN1Yq7/1Ocstc1lUEm+AT+/rCkibIcC/OmWo5f0EA48Vf8CytHzGrSwbQ\" crossorigin=\"anonymous")]

//! # tinguely
//!
//! tinguely is a machine learning library implemented entirely in Rust.
//! This library is still in early stages of development.
//!
//! ## Usage
//!
//! The library usage is described well in the API documentation - including example code.
//!
//!
//! Add this to your `Cargo.toml`:
//!
//! ```toml
//! [dependencies.tinguely]
//! version = "0.2"
//! default-features = false
//! features = [openblas]
//! ```
//!
//! Then import the modules and it is ready to be used:
//!
//! ``` rust
//! use tinguely as tg;
//!
//! ```
//! ## Features
//!
//! * Regression
//! 	* Linear regression
//! * Clustering
//! 	* DBSCAN
//! 	* K-Means
//! * Classification
//! 	* Logistic regression
//! 	* Support vector machine
pub mod clustering;
pub mod model;
pub mod regression;
pub mod classification;

pub use model::SupervisedLearn;
pub use model::UnsupervisedLearn;

